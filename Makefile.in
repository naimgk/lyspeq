#---------------------------------------
# GSL recommends the following flags
# removed flags: -ansi -Wmissing-prototypes -Wstrict-prototypes -Wconversion -Wnested-externs -Dinline=
# GSLRECFLAGS=-Wall -pedantic -Werror -W -Wno-unknown-pragmas\
# -Wshadow -Wpointer-arith \
# -Wcast-qual -Wcast-align -Wwrite-strings \
# -fshort-enums -fno-common
				
#---------------------------------------
DIRS=$(addprefix $(srcdir)/, core io gsltools tests)
DEPDIR=$(addprefix $(srcdir)/, dep)
$(shell mkdir -p $(DEPDIR))
SRCEXT=cpp
VPATH=$(srcdir):$(DIRS)

SOURCES=fiducial_cosmology.cpp global_numbers.cpp matrix_helper.cpp one_qso_estimate.cpp \
quadratic_estimate.cpp sq_table.cpp discrete_interpolation.cpp fourier_integrator.cpp \
interpolation.cpp interpolation_2d.cpp config_file.cpp io_helper_functions.cpp qso_file.cpp \
sq_lookup_table_file.cpp logger.cpp real_field.cpp chunk_estimate.cpp

OBJECTS=$(SOURCES:.$(SRCEXT)=.o)
bin_OBJ=LyaPowerEstimate.o CreateSQLookUpTable.o cblas_tests.o testPointRef.o testSQCMatrices.o \
test-fftconvolve.o myfitsread.o
DEPS=$(addprefix $(DEPDIR)/, $(OBJECTS:.o=.d) $(bin_OBJ:.o=.d))

# -DHAVE_INLINE for inline declarations in GSL for faster performance
CPPFLAGS+=-I$(srcdir) -std=gnu++11 -DHAVE_INLINE $(MORECPPFLAGS)
CPPFLAGS+=-MT $@ -MMD -MP -MF $(DEPDIR)/$*.d
CXXFLAGS=$(OPT) $(GSLRECFLAGS)

all: cblas_tests testSQCMatrices LyaPowerEstimate CreateSQLookUpTable myfitsread test-fftconvolve

test: all
	mkdir -p $(srcdir)/tests/output
	
	@echo "Testing CBLAS functions...."
	@./cblas_tests > $(srcdir)/tests/output/output_cblas.txt 
	@diff $(srcdir)/tests/output/output_cblas.txt $(srcdir)/tests/truth/expected_cblas_output.txt

	@echo "Producing S & Q lookup tables..."
	@${exec}CreateSQLookUpTable $(srcdir)/tests/input/test.config > $(srcdir)/tests/output/CreateSQLookUpTable.log

	@echo "Producing interpolated S & Q matrices..."
	@${exec}testSQCMatrices $(srcdir)/tests/input/test.config > $(srcdir)/tests/output/testSQCMatrices.log

	@echo "Running QMLE..."
	@${exec}LyaPowerEstimate $(srcdir)/tests/input/test.config > $(srcdir)/tests/output/LyaPowerEstimate.log

	@python3 tests/compareTestResults.py $(srcdir)
	
LyaPowerEstimate: LyaPowerEstimate.o $(OBJECTS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

CreateSQLookUpTable: CreateSQLookUpTable.o $(OBJECTS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

cblas_tests: cblas_tests.o matrix_helper.o real_field.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

testSQCMatrices: testSQCMatrices.o $(OBJECTS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

testPointRef: testPointRef.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

myfitsread: myfitsread.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

test-fftconvolve: test-fftconvolve.o $(OBJECTS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $^ -o $@ $(LDFLAGS) $(LDLIBS)

.PHONY: install uninstall clean

install: LyaPowerEstimate CreateSQLookUpTable
	mkdir -p $(bindir)
	cp LyaPowerEstimate CreateSQLookUpTable $(srcdir)/py/lorentzian_fit.py $(srcdir)/py/smbivspline.py $(bindir)
	chmod a+x $(bindir)/lorentzian_fit.py $(bindir)/smbivspline.py

uninstall:
	$(RM) $(bindir)/lorentzian_fit.py
	$(RM) $(bindir)/LyaPowerEstimate $(bindir)/CreateSQLookUpTable

clean:
	$(RM) $(OBJECTS) $(bin_OBJ)
	$(RM) LyaPowerEstimate CreateSQLookUpTable cblas_tests

deepclean:
	$(RM) $(OBJECTS) $(bin_OBJ) $(DEPS)
	$(RM) LyaPowerEstimate CreateSQLookUpTable cblas_tests

-include $(DEPS)
